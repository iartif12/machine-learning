![Men Working](https://gitlab.com/oemunoz/machine-learning/raw/master/images/menworking.png "Document under maintenance!")

Warnings:
There are some attempts at humor here.

----

# Augmented Reality / Image Recognition

![Google search](https://gitlab.com/oemunoz/machine-learning/raw/master/images/machine_learning.png "Google search: Machine Learning")

----

# Moodstocks

[![Moodstocks](https://gitlab.com/oemunoz/machine-learning/raw/master/images/modstoks.png)](https://moodstocks.com/)

----

## Augmented Reality / Image Recognition

[![RNN](https://gitlab.com/oemunoz/machine-learning/raw/master/images/RNN.png "Google search: Machine Learning")](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)


- Image caption
- Secuence classfication
- Translation
- Named entity recognition

_
====

[![Supervised Learning](https://gitlab.com/oemunoz/machine-learning/raw/master/images/SupervisedLearning.png "Supervised Learning")](http://www.slideshare.net/holbertonschool/deep-learning-keynote-1-by-louis-monier)

- Bluemix IBM
- Moodstocks
- Vuforia
- Wikitude
- Wazzat Labs

_
====

----

# Demo Bluemix IBM!

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/ibmwatson.jpg "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

# Flow of the service

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/vr-process2.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

# Use cases

- **Manufacturing:** Use images from a manufacturing setting to make sure products are being positioned correctly on an assembly line
- **Visual Auditing:** Look for visual compliance or deterioration in a fleet of trucks, planes, or windmills out in the field, train custom classifiers to understand what defects look like
- **Insurance:** Rapidly process claims by using images to classify claims into different categories.
- **Social listening:** Use images from your product line or your logo to track buzz about your company on social media
- **Social commerce:** Use an image of a plated dish to find out which restaurant serves it and find reviews, use a travel photo to find vacation suggestions based on similar experiences, use a house image to find similar homes that are for sale
- **Retail:** Take a photo of a favorite outfit to find stores with those clothes in stock or on sale, use a travel image to find retail suggestions in that area
- **Education:** Create image-based applications to educate about taxonomies, use pictures to find educational material on similar subjects

_
====

# Demo Bluemix IBM

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/bluemixDemo.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

# Demo Bluemix IBM

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/bluemixDemo1.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

# Demo Bluemix IBM

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/bluemixDemo2.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

# Demo Bluemix IBM !!!!!

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/bluemixDemo3.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

----

# IBM Watson Services

- AlchemyLanguage
- AlchemyVision
- AlchemyData News
- Authorization
- Concept Insights

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

# IBM Watson Services

- Conversation
- Dialog
- Document Conversion
- Language Translator
- Natural Language Classifier

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

# IBM Watson Services

- Personality Insights
- Relationship Extraction
- Retrieve and Rank
- Speech to Text
- Text to Speech

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

# IBM Watson Services

- Tone Analyzer
- Tradeoff Analytics
- Visual Insights
- Visual Recognition

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

----

### [ConvNetJS](http://cs.stanford.edu/people/karpathy/convnetjs/) Deep Q Learning Demo

This demo follows the description of the Deep Q Learning algorithm described in [Playing Atari with Deep Reinforcement Learning](http://arxiv.org/pdf/1312.5602v1.pdf), a paper from NIPS 2013 Deep Learning Workshop from DeepMind. The paper is a nice demo of a fairly standard (model-free) Reinforcement Learning algorithm (Q Learning) learning to play Atari games.

_
====

<iframe  frameborder=0 width="900" height="700" marginheight=0 marginwidth=0 scrolling="auto" src="http://www.pepitosoft.com/demos/rldemo/index.html"></iframe>

----

## Useful, Reference and Future links

- [BlueMix](https://www.google.com.co/aclk?sa=L&ai=DChcSEwiDvJ715rDQAhVXG4EKHXicAzgYABAA&sig=AOD64_2dC71QClj6e8QGg02JsbBbGDHEsQ&q=&ved=0ahUKEwi695j15rDQAhUF5SYKHbP8BP4Q0QwIFw&adurl=)
- [Chromium](https://chromium.googlesource.com/chromium/src.git/+/master/components/neterror/resources/offline.js)
- [opencv](http://docs.opencv.org/2.4/doc/tutorials/introduction/display_image/display_image.html)
- [robotjs](https://github.com/octalmage/robotjs)
- [IAMDinosaur](https://github.com/ivanseidel/IAMDinosaur)
- [synaptic](http://caza.la/synaptic/#/)

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- [Code Google.](https://code.google.com/p/chromium/codesearch#chromium/src/components/neterror/resources/offline.js&q=offline.js&sq=package:chromium&type=cs)
- [wikitude](http://www.wikitude.com/blog-migrating-moodstocks-heres-keep-app-online/)
- [moodstocks](https://moodstocks.com/)
- [tngan](https://github.com/tngan)
- [T-Rex](https://github.com/amaneureka/T-Rex/)

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- [convnetjs](http://cs.stanford.edu/people/karpathy/convnetjs/)
- [THE MNIST DATABASE](http://yann.lecun.com/exdb/mnist/)
- [Neural Networks in Javascript](http://blog.webkid.io/neural-networks-in-javascript/)
- [Top Machine Learning Libraries for Javascript](http://www.kdnuggets.com/2016/06/top-machine-learning-libraries-javascript.html)

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- [Playing Atari with Deep Reinforcement Learning](https://arxiv.org/pdf/1312.5602v1.pdf)
- [Understanding LSTM Networks](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)
- [5 Machine Learning Projects You Can No Longer Overlook](http://www.kdnuggets.com/2016/05/five-machine-learning-projects-cant-overlook.html)
- [Top 10 Machine Learning Projects on Github](http://www.kdnuggets.com/2015/12/top-10-machine-learning-github.html/2)

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- [Lian Li: Machine Learning with Node.js - JSUnconf 2016](https://www.youtube.com/watch?v=M5glN6XjDv8)
- [Machine learning is not the future - Google I/O 2016](https://www.youtube.com/watch?v=3dXQxSI3XDY)
- [Machine Learning Tutorial for JavaScript Hackers: Tips On Gathering and Pre-Processing Data](https://www.youtube.com/watch?v=5AGM7grR680)
- [ConvNetJS – Deep Learning in your browser](https://www.youtube.com/watch?v=nAHcrz5hxc4)

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- [Deep Learning Frameworks Compared](https://www.youtube.com/watch?v=MDP9FfsNx60)
- [7.1 Recurrent Neural Networks RNN](https://www.youtube.com/watch?v=kMLl-TKaEnc)
- []()
- []()

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- [IBM Watson: Using the Visual Recognition service ](https://www.youtube.com/watch?v=FYZld6SSCnY)
- [Neural Networks in JavaScript](http://www.antoniodeluca.info/blog/10-08-2016/neural-networks-in-javascript.html)
- [Introduction to Artificial Neural Networks](http://www.theprojectspot.com/tutorial-post/introduction-to-artificial-neural-networks-part-1/7)
- [A 'Brief' History of Neural Nets and Deep Learning](http://www.andreykurenkov.com/writing/a-brief-history-of-neural-nets-and-deep-learning/)

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- [Synaptic - Advanced Neural Nets In JavaScript](http://www.i-programmer.info/news/105-artificial-intelligence/7890-synaptic-advanced-neural-nets-in-javascript.html)
- []()
- []()
- []()

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- []()
- []()
- []()
- []()

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)
