![Men Working](https://gitlab.com/oemunoz/machine-learning/raw/master/images/menworking.png "Document under maintenance!")

Warnings:
There are some attempts at humor here.

----

# Recurrent Neural Networks (RNN)

![Google search](https://gitlab.com/oemunoz/machine-learning/raw/master/images/machine_learning.png "Google search: Machine Learning")

----

## Long-Short Term Memory (LSTM) Network

### Recurrent Neural Networks (RNN)

[![RNN](https://gitlab.com/oemunoz/machine-learning/raw/master/images/RNN.png "Google search: Machine Learning")](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)

- Image caption
- Secuence classfication
- Translation
- Named entity recognition

_
====

### Recurrent Neural Networks have loops

Humans don’t start their thinking from scratch every second.

[![RNN](https://gitlab.com/oemunoz/machine-learning/raw/master/images/RNN-rolled.png "Understanding LSTM Networks")](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)

Recurrent neural networks address this issue. They are networks with loops in them, allowing information to persist.

_
====

### An unrolled recurrent neural network.

A recurrent neural network can be thought of as multiple copies of the same network, each passing a message to a successor.

[![RNN](https://gitlab.com/oemunoz/machine-learning/raw/master/images/RNN-unrolled.png "Understanding LSTM Networks")](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)

_
====

### The Problem of Long-Term Dependencies

[![RNN](https://gitlab.com/oemunoz/machine-learning/raw/master/images/RNN-shorttermdepdencies.png "Understanding LSTM Networks")](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)

Predict the next word:
- the clouds are in the **sky**,
- I grew up in France… I speak fluent **French**.

_
====

## LSTM Networks

Long Short Term Memory networks – usually just called “LSTMs” – are a special kind of RNN, capable of learning long-term dependencies.

_
====

### The repeating module in a standard RNN contains a single layer.

[![RNN](https://gitlab.com/oemunoz/machine-learning/raw/master/images/LSTM3-SimpleRNN.png "Understanding LSTM Networks")](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)

_
====

### The repeating module in an LSTM contains four interacting layers.

[![RNN](https://gitlab.com/oemunoz/machine-learning/raw/master/images/LSTM3-chain.png "Understanding LSTM Networks")](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)

[![RNN](https://gitlab.com/oemunoz/machine-learning/raw/master/images/LSTM2-notation.png "Understanding LSTM Networks")](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)

_
====

### More Neurons,Layers: More Deep Learning.

[![Deep Learning
](https://gitlab.com/oemunoz/machine-learning/raw/master/images/MoreNeuronsLayers.png "Deep Learning")](http://www.slideshare.net/holbertonschool/deep-learning-keynote-1-by-louis-monier)

----

**Node.js**

Es un entorno de ejecución para JavaScript basado en el motor V8 de Google y actualmente cuenta con el ecosistema de librerias de código abierto mas grande del mundo llamada NPM. Es un entorno de ejecución orientado a eventos.

![](https://gitlab.com/iush/blue-doc/raw/master/images/nodejs.jpg)

_
====

**Qué es y para que sirve Node.js?**

Node.js es un entorno de ejecución de JavaScript de lado del servidor, el cual permite la creación de aplicaciones de red altamente escalables.

![](https://gitlab.com/iush/blue-doc/raw/master/images/yellowNodeJs.png)

_
====

**Qué es y para que sirve Node.js?**

Se basa en Programación Orientada a Eventos Asíncronos. Node.js cambia la manera de como conectarse a un servidor, ya que cada conexión dispara un evento especifico dentro del motor y se afirma que puede soportar decenas de miles de conexiones concurrentes sin bloqueos.

![](https://gitlab.com/iush/blue-doc/raw/master/images/yellowNodeJs.png)

_
====

**JavaScript en el servidor**

- Lenguaje de programación potente

- Posee un excelente modelo de eventos asincrónicos

- Curva de aprendizaje reducida debido al previo conocimiento de muchos desarrolladores

![](https://gitlab.com/iush/blue-doc/raw/master/images/yellowNodeJs.png)

_
====

**Ventajas**

- Gran capacidad de tolerancia a la cantidad de peticiones simultaneas

- Existencia de gran cantidad de módulos para casi todas las necesidades que se tengan gracias a NPM

- Menor costo de infraestructura

- Comunidad creciente dispuesta a resolver dudas y mostrar nuevas maneras de uso

![](https://gitlab.com/iush/blue-doc/raw/master/images/nodeacua.png)

----

## Demo [IAMDinosaur](https://www.youtube.com/watch?v=P7XHzqZjXQs)

[![IAMDinosaur](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaur.png "Google search: Machine Learning")](https://github.com/ivanseidel/IAMDinosaur)

_
====

### Chrome/Chromium offline script

[![Synaptic](https://gitlab.com/oemunoz/machine-learning/raw/master/images/trex.png "Ivan Seidel Learning")](http://caza.la/synaptic/#/)

- [Chromium](https://chromium.googlesource.com/chromium/src.git/+/master/components/neterror/resources/offline.js)
- [Code Google.](https://code.google.com/p/chromium/codesearch#chromium/src/components/neterror/resources/offline.js&q=offline.js&sq=package:chromium&type=cs)

_
====

### Synaptic

[![Synaptic](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaur4.png "Ivan Seidel Learning")](http://caza.la/synaptic/#/)

_
====

### RobotJs

[![RobotJs](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaur5.png "Maintained by Jason Stallings.")](https://github.com/octalmage/robotjs)

_
====

### Sensors

[![IAMDinosaur](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaur1.png "Ivan Seidel Learning")](http://www.slideshare.net/NodejsFoundation/from-pterodactyls-and-cactus-to-artificial-intelligence-ivan-seidel-gomes-tenda-digital-66868648)

_
====

### Actuators

[![IAMDinosaur](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaur2.png "Ivan Seidel Learning")](http://www.slideshare.net/NodejsFoundation/from-pterodactyls-and-cactus-to-artificial-intelligence-ivan-seidel-gomes-tenda-digital-66868648)

_
====

### Neural Network

The distance affects the input

[![IAMDinosaur](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaurGeneTraduction.png "Ivan Seidel Learning")](http://www.slideshare.net/NodejsFoundation/from-pterodactyls-and-cactus-to-artificial-intelligence-ivan-seidel-gomes-tenda-digital-66868648)

_
====

[![IAMDinosaur](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaur3.png "Ivan Seidel Learning")](http://www.slideshare.net/NodejsFoundation/from-pterodactyls-and-cactus-to-artificial-intelligence-ivan-seidel-gomes-tenda-digital-66868648)

_
====


[![IAMDinosaur](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaurLAllGeneration1.png "Ivan Seidel Learning")](http://www.slideshare.net/NodejsFoundation/from-pterodactyls-and-cactus-to-artificial-intelligence-ivan-seidel-gomes-tenda-digital-66868648)

_
====

### Genetic Algorithm

[![IAMDinosaur](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaur6.png "Ivan Seidel Learning")](http://www.slideshare.net/NodejsFoundation/from-pterodactyls-and-cactus-to-artificial-intelligence-ivan-seidel-gomes-tenda-digital-66868648)

_
====

[![IAMDinosaur](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaur7.png "Ivan Seidel Learning")](http://www.slideshare.net/NodejsFoundation/from-pterodactyls-and-cactus-to-artificial-intelligence-ivan-seidel-gomes-tenda-digital-66868648)

_
====

[![IAMDinosaur](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaurGenes.png "Ivan Seidel Learning")](http://www.slideshare.net/NodejsFoundation/from-pterodactyls-and-cactus-to-artificial-intelligence-ivan-seidel-gomes-tenda-digital-66868648)

_
====

[![IAMDinosaur](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaurSelect.png "Ivan Seidel Learning")](http://www.slideshare.net/NodejsFoundation/from-pterodactyls-and-cactus-to-artificial-intelligence-ivan-seidel-gomes-tenda-digital-66868648)

_
====

[![IAMDinosaur](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaurMutante1.png "Ivan Seidel Learning")](http://www.slideshare.net/NodejsFoundation/from-pterodactyls-and-cactus-to-artificial-intelligence-ivan-seidel-gomes-tenda-digital-66868648)

_
====

[![IAMDinosaur](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaurMutante.png "Ivan Seidel Learning")](http://www.slideshare.net/NodejsFoundation/from-pterodactyls-and-cactus-to-artificial-intelligence-ivan-seidel-gomes-tenda-digital-66868648)

_
====

[![IAMDinosaur](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaurLAllGeneration.png "Ivan Seidel Learning")](http://www.slideshare.net/NodejsFoundation/from-pterodactyls-and-cactus-to-artificial-intelligence-ivan-seidel-gomes-tenda-digital-66868648)

----

## Demo T-Rex

<section>
<iframe  frameborder=0 width="630" height="170" marginheight=0 marginwidth=0 scrolling="auto" src="http://pepitosoft.gitlab.io/demos/trex/index.html"></iframe>
[T-Rex](https://github.com/amaneureka/T-Rex/)
</section>

_
====

### Chrome/Chromium offline script

[![Synaptic](https://gitlab.com/oemunoz/machine-learning/raw/master/images/trex.png "Ivan Seidel Learning")](http://caza.la/synaptic/#/)

- [Chromium](https://chromium.googlesource.com/chromium/src.git/+/master/components/neterror/resources/offline.js)
- [Code Google.](https://code.google.com/p/chromium/codesearch#chromium/src/components/neterror/resources/offline.js&q=offline.js&sq=package:chromium&type=cs)

_
====

### Synaptic

[![Synaptic](https://gitlab.com/oemunoz/machine-learning/raw/master/images/iamdinosaur4.png "Ivan Seidel Learning")](http://caza.la/synaptic/#/)

----

## Useful, Reference and Future links

- [BlueMix](https://www.google.com.co/aclk?sa=L&ai=DChcSEwiDvJ715rDQAhVXG4EKHXicAzgYABAA&sig=AOD64_2dC71QClj6e8QGg02JsbBbGDHEsQ&q=&ved=0ahUKEwi695j15rDQAhUF5SYKHbP8BP4Q0QwIFw&adurl=)
- [Chromium](https://chromium.googlesource.com/chromium/src.git/+/master/components/neterror/resources/offline.js)
- [opencv](http://docs.opencv.org/2.4/doc/tutorials/introduction/display_image/display_image.html)
- [robotjs](https://github.com/octalmage/robotjs)
- [IAMDinosaur](https://github.com/ivanseidel/IAMDinosaur)
- [synaptic](http://caza.la/synaptic/#/)

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- [Code Google.](https://code.google.com/p/chromium/codesearch#chromium/src/components/neterror/resources/offline.js&q=offline.js&sq=package:chromium&type=cs)
- [wikitude](http://www.wikitude.com/blog-migrating-moodstocks-heres-keep-app-online/)
- [moodstocks](https://moodstocks.com/)
- [tngan](https://github.com/tngan)
- [T-Rex](https://github.com/amaneureka/T-Rex/)

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- [convnetjs](http://cs.stanford.edu/people/karpathy/convnetjs/)
- [THE MNIST DATABASE](http://yann.lecun.com/exdb/mnist/)
- [Neural Networks in Javascript](http://blog.webkid.io/neural-networks-in-javascript/)
- [Top Machine Learning Libraries for Javascript](http://www.kdnuggets.com/2016/06/top-machine-learning-libraries-javascript.html)

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- [Playing Atari with Deep Reinforcement Learning](https://arxiv.org/pdf/1312.5602v1.pdf)
- [Understanding LSTM Networks](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)
- [5 Machine Learning Projects You Can No Longer Overlook](http://www.kdnuggets.com/2016/05/five-machine-learning-projects-cant-overlook.html)
- [Top 10 Machine Learning Projects on Github](http://www.kdnuggets.com/2015/12/top-10-machine-learning-github.html/2)

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- [Lian Li: Machine Learning with Node.js - JSUnconf 2016](https://www.youtube.com/watch?v=M5glN6XjDv8)
- [Machine learning is not the future - Google I/O 2016](https://www.youtube.com/watch?v=3dXQxSI3XDY)
- [Machine Learning Tutorial for JavaScript Hackers: Tips On Gathering and Pre-Processing Data](https://www.youtube.com/watch?v=5AGM7grR680)
- [ConvNetJS – Deep Learning in your browser](https://www.youtube.com/watch?v=nAHcrz5hxc4)

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- [Deep Learning Frameworks Compared](https://www.youtube.com/watch?v=MDP9FfsNx60)
- [7.1 Recurrent Neural Networks RNN](https://www.youtube.com/watch?v=kMLl-TKaEnc)
- []()
- []()

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- [IBM Watson: Using the Visual Recognition service ](https://www.youtube.com/watch?v=FYZld6SSCnY)
- [Neural Networks in JavaScript](http://www.antoniodeluca.info/blog/10-08-2016/neural-networks-in-javascript.html)
- [Introduction to Artificial Neural Networks](http://www.theprojectspot.com/tutorial-post/introduction-to-artificial-neural-networks-part-1/7)
- [A 'Brief' History of Neural Nets and Deep Learning](http://www.andreykurenkov.com/writing/a-brief-history-of-neural-nets-and-deep-learning/)

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- [Synaptic - Advanced Neural Nets In JavaScript](http://www.i-programmer.info/news/105-artificial-intelligence/7890-synaptic-advanced-neural-nets-in-javascript.html)
- [A Beginner’s Guide to Recurrent Networks and LSTMs](https://deeplearning4j.org/lstm)
- []()
- []()

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)

_
====

## Useful, Reference and Future links

- []()
- []()
- []()
- []()

[![Super Demo IBM](https://gitlab.com/oemunoz/machine-learning/raw/master/images/watson_node.png "BlueMix IBM!")](http://oems-vis-rec-demo.mybluemix.net/)
